import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User01 } from './user.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectModel('User01') private readonly userModel: Model<User01>,
  ) {}

  async create(dto: User01) {
    return this.userModel.create(dto);
  }

  async getById(ids: string) {
    return this.userModel.findOne({ id: ids });
  }

  async findAll() {
    return this.userModel.find();
  }

  update(id: string, updateUserDto: User01) {
    return this.userModel.findByIdAndUpdate(id, updateUserDto);
  }

  delete(id: string) {
    return this.userModel.findByIdAndDelete(id);
  }
}
