import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { AppService } from './app.service';
import { User01 } from './user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Post()
  async create(@Body() dto: User01) {
    return this.appService.create(dto);
  }

  @Get(':ids')
  async get(@Param('ids') ids: string) {
    return this.appService.getById(ids);
  }

  @Get()
  findAll() {
    return this.appService.findAll();
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: User01) {
    return this.appService.update(id, updateUserDto);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.appService.delete(id);
  }
}
